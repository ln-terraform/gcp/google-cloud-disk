output "id" {
  value = google_compute_disk.disk.id
}

output "self_link" {
  value = google_compute_disk.disk.self_link
}