variable "project" {
  type = string
}

variable "name" {
  type = string
}

variable "type" {
  type    = string
  default = "pd-balanced"
}

variable "zone" {
  type = string
}

variable "labels" {
  type    = map(string)
  default = {}
}

variable "physical_block_size_bytes" {
  type    = number
  default = 4096
}

variable "size" {
  type    = number
  default = 100
}

variable "snapshot" {
  type    = string
  default = null
}
