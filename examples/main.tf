module "google-cloud-disk" {
  source = "../"

  project                   = var.project
  zone                      = "asia-southeast2-a"
  name                      = "test-disk"
  type                      = "pd-balanced"
  size                      = 50
  labels = {
    terraform   = "true"
    environment = "test"
  }
}