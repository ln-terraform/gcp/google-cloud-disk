resource "google_compute_disk" "disk" {
  project                   = var.project
  zone                      = var.zone
  name                      = var.name
  type                      = var.type
  size                      = var.size
  physical_block_size_bytes = var.physical_block_size_bytes
  labels                    = var.labels
}